#!/usr/bin/env node

import esbuild from "esbuild"
import { watch } from "fs/promises"
import globby from "globby"
import { join } from "path"
import replaceExt from "replace-ext"

const src = process.cwd()

const [, , ...patterns] = process.argv

;(async () => {
  try {
    if (patterns.length === 0) {
      patterns.push("**/*.ts")
    }
    const files = await globby(patterns, {
      ignore: ["node_modules", "**/*.d.ts"],
      cwd: src,
    })
    const watchers = []
    for (const file of files) {
      const realFile = join(src, file)
      watchers.push(
        (async () => {
          const watcher = watch(realFile)
          for await (const { eventType } of watcher) {
            if (eventType === "change") {
              try {
                await esbuild.build({
                  entryPoints: [realFile],
                  outfile: replaceExt(realFile, ".js"),
                  format: "cjs",
                  target: "es2020",
                })
              } catch (err) {
                console.error(err)
              }
            }
          }
        })()
      )
    }
    console.log(`Watching ${watchers.length} files.`)
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
})()
