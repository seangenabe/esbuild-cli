#!/usr/bin/env node

import esbuild from "esbuild"
import globby from "globby"
import pMap from "p-map"
import { join } from "path"
import replaceExt from "replace-ext"

const src = process.cwd()

const [, , ...patterns] = process.argv

;(async () => {
  try {
    if (patterns.length === 0) {
      patterns.push("**/*.ts")
    }
    const files = await globby(patterns, {
      ignore: ["node_modules", "**/*.d.ts"],
      cwd: src,
    })
    await pMap(
      files,
      async (file) => {
        const realFile = join(src, file)
        await esbuild.build({
          entryPoints: [realFile],
          outfile: replaceExt(realFile, ".js"),
          sourcemap: true,
          tsconfig: `${process.cwd()}/tsconfig.json`,
          format: "cjs",
          target: "es2020",
        })
      },
      { concurrency: 4 }
    )
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
})()
