# esbuild CLI

Build TypeScript packages with esbuild.

## Usage

```sh
esbuild-build [...patterns]
esbuild-watch [...patterns]
```

Arguments:

* patterns - One or more patterns to glob for files. Powered by [globby](https://www.npmjs.com/package/globby).

Working directory:

Your working directory must have a `tsconfig.json` file.
